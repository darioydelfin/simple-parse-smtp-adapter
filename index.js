"use strict";
const path = require('path');
const nodemailer = require("nodemailer");
const EmailTemplate = require('email-templates').EmailTemplate;

let SimpleParseSmtpAdapter = (adapterOptions) => {
    if (!adapterOptions || !adapterOptions.user || !adapterOptions.password || !adapterOptions.host || !adapterOptions.fromAddress ) {
        throw 'SimpleParseSMTPAdapter requires user, password, host, and fromAddress';
    }

    /**
     * Creates trasporter for send emails
     */
    let transporter = nodemailer.createTransport({
        host: adapterOptions.host,
        port: adapterOptions.port,
        secure: adapterOptions.isSSL,
        name: adapterOptions.name || '127.0.0.1',
        auth: {
            user: adapterOptions.user,
            pass: adapterOptions.password
        },
        tls: {
            rejectUnauthorized: adapterOptions.isTlsRejectUnauthorized !== undefined ? adapterOptions.isTlsRejectUnauthorized : true
        }
    });

    /**
     * When emailField is defined in adapterOptines return that field
     * if not return the field email and if is undefined returns username
     * 
     * @param Parse Object user
     * @return String email
     */
    let getUserEmail = (user) => {
        let email = user.get('email') || user.get('username');

        if (adapterOptions.emailField) {
            email = user.get(adapterOptions.emailField);
        }

        return email;
    };

    /**
     * Return an email template with data rendered using email-templates module
     * check module docs: https://github.com/niftylettuce/node-email-templates
     *
     * @param String template path template
     * @param Object data object with data for use in template
     */
    let renderTemplate = (template, data) => {
        let templateDir = template;
        let html = new EmailTemplate(templateDir);

        return new Promise((resolve, reject) => {
            html.render(data, (err, result) => {
                if (err) {
                    console.log(err)
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    };

    /**
     * Parse use this function by default for sends emails
     * @param mail This object contain to address, subject and email text in plain text
     * @returns {Promise}
     */
    let sendMail = (mail) => {
        let mailOptions = {
            to: mail.to,
            html: mail.text,
            subject: mail.subject,
            from: adapterOptions.fromAddress
        };

        return new Promise((resolve, reject) => {
            transporter.sendMail(mailOptions, (error, info) => {
                if(error) {
                    console.log(error)
                    reject(error);
                } else {
                    resolve(info);
                }
            });
        });
    };

    /**
     * When this method is available parse use for send email for reset password
     * @param data This object contain {appName}, {link} and {user} user is an object parse of User class
     * @returns {Promise}
     */
    let sendPasswordResetEmail = async(data) => {
        let mail = {
            subject: 'Reset Password',
            to: getUserEmail(data.user)
        };
	const username = await data.user.get('username')
	console.log('aqui')
        if (adapterOptions.templates && adapterOptions.templates.resetPassword) {

	    console.log('mas alla')
            mail.text=`<html xmlns="http://www.w3.org/1999/xhtml">

            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <style>
                    body{
                        margin: 0;
                        padding: 0;
                    }
                    .header{
                        background-color: #30408C;
                        height: 70px;
                    }
                    .header img{
                        height: 50px;
                        margin: 10px;
                    }
                    .table{
                        margin-left: 50px;
                    }
                </style>
            </head>
            
            <body style="width: 100% !important; -webkit-text-size-adjust: none; margin: 0; ">
                <div class="header">
                    <img src="https://webv3.yacubo.com/assets/img/logo-1.svg" alt="">
                </div>
                <div class="table">
            
                    <table
                        style="border-spacing: 0; border-collapse: collapse; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; width: 100% !important; height: 100% !important; color: #4c4c4c; font-size: 15px; line-height: 150%; background: #ffffff; margin: 0; padding: 0; border: 0;">
                        <tr style="vertical-align: top; padding: 0;">
                            <td valign="top" style="vertical-align: top; padding: 0;">
                                <p style="margin: 20px 0;">Hola ${username},</p>
                                <p style="margin: 20px 0;">Haga clic en el botón de abajo para establecer una nueva contraseña:</p>
                                <p style="margin: 20px 0;"><a id="link"
                                        style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #ffffff; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #30408C; margin: 0; border-color: #30408C; border-style: solid; border-width: 10px 20px;"
                                        href="${data.link}">Restablecer contraseña</a></p>
                                <p style="margin: 20px 0;">Si no desea restablecer su contraseña, ignore este correo electrónico y no se
                                    tomarán medidas.</p>
                            </td>
                        </tr>
                    </table>
                </div>
            </body>
            
            </html>`
//'<h1> este es tu enlace</h1><a href="'+data.link+'">resetea aqui</a>'
            return sendMail(mail);

	    // return renderTemplate(adapterOptions.templates.resetPassword.template, data).then((result) => {
            //     mail.text = result.html;
            //     mail.subject = adapterOptions.templates.resetPassword.subject;

            //     return sendMail(mail);
            // }, (e) => {

            //     return new Promise((resolve, reject) => {
            //         console.log(e)
            //         reject(e);
            //     });
            // });

        } else {
            mail.text = data.link;

            return sendMail(mail);
        }
    };

    /**
     * When this method is available parse use for send email for email verification
     * @param data This object contain {appName}, {link} and {user} user is an object parse of User class
     * @returns {Promise}
     */
    let sendVerificationEmail = (data) => {
        let mail = {
            subject: 'Verify Email',
            to: getUserEmail(data.user)
        };

        if (adapterOptions.templates && adapterOptions.templates.verifyEmail) {

            return renderTemplate(adapterOptions.templates.verifyEmail.template, data).then((result) => {
                mail.text = result.html;
                mail.subject = adapterOptions.templates.verifyEmail.subject;

                return sendMail(mail);
            }, (e) => {

                return new Promise((resolve, reject) => {
                    console.log(e);
                    reject(e);
                });
            });

        } else {
            mail.text = data.link;

            return sendMail(mail);
        }
    };

    return Object.freeze({
        sendMail: sendMail,
        sendPasswordResetEmail: sendPasswordResetEmail,
        sendVerificationEmail: sendVerificationEmail
    });
};

module.exports = SimpleParseSmtpAdapter;
